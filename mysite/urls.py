from django.urls import path
from mysite import views
app_name="mysite"
urlpatterns = [
    path('about/',views.about,name='about'),
    path('contact/',views.contact,name='contact'),

    path('showproducts/',views.showproducts,name='showproducts'),
    path('uslogin/',views.uslogin,name='uslogin'),
    path('usregister/',views.usregister,name='usregister'),
    path('single/',views.single,name='single'),
    path('add_review/',views.add_review,name='add_review'),
    path('cart/',views.addcart,name='cart'),
    path('removecart/',views.removeCart,name='removecart'),
    path('grandTotal/',views.grandTotal,name='grandTotal'),
    path('success_payment/',views.success_payment, name='success_payment'),
    path('get_order/',views.get_order, name='get_order'),
    path('order_summary/',views.order_summary, name='order_summary'),
    path('order_history/',views.order_history, name='order_history'),

    path('dashboard/',views.dashboard,name='dashboard'),
    path('profile/',views.profile, name='profile'),
    path('change_profile/',views.change_profile, name='change_profile'),
    path('change_password/',views.change_password, name='change_password'),
    path('uservalid/',views.uservalid, name='uservalid'),

    path('logout/',views.uslogout,name='uslogout'),



]